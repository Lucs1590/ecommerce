import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientesModule } from './modules/clientes/clientes.module';
import { ProdutosModule } from './modules/produtos/produtos.module';
import { LayoutModule } from './layout/layout.module';

const routes: Routes = [
  { path: '', redirectTo: 'produtos', pathMatch: 'full' },
  { path: '*', redirectTo: 'produtos', pathMatch: 'full' },
  { path: 'clientes', loadChildren: './modules/clientes/clientes.module#ClientesModule'  },
  { path: 'produtos', loadChildren: './modules/produtos/produtos.module#ProdutosModule' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes),
    ClientesModule,
    ProdutosModule,
    LayoutModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

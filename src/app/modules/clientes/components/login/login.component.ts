import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ApiService } from 'src/app/core/services/api.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/core/services/util.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  subscription: Subscription;
  loginForm: FormGroup;
  submited = false;

  constructor(
    private apiService: ApiService,
    private router: Router,
    public utils: UtilService
  ) {
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.required),
      senha: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
  }

  get form() { return this.loginForm.controls; }

  submit() {
    this.submited = true;

    if (this.loginForm.invalid) {
      return;
    } else {
      this.logar();
    }
  }

  logar() {
    const body = this.loginForm.value;
    console.log(body);
    const login = this.apiService.login(body).subscribe(data => {
      // tslint:disable: no-string-literal
      localStorage.setItem('idUsuario', data['id']);
      localStorage.setItem('permicao', data['tipo']);
      localStorage.setItem('nomeCliente', data['nome']);
      this.utils.verificarLogado();
      this.router.navigate(['/produtos']);
    }, error => {
      console.error(error);
    });
  }
}

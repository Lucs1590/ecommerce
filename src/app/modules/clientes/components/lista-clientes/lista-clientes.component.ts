import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ApiService } from 'src/app/core/services/api.service';
import { Subscription } from 'rxjs';
import { UtilService } from 'src/app/core/services/util.service';
@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.css']
})
export class ListaClientesComponent implements OnInit {
  subscription: Subscription;
  clientForm: FormGroup;
  submited = false;
  listaDeClientes;
  interval;
  statusEnvio;

  constructor(
    private apiService: ApiService,
    public utils: UtilService,
    public router: Router
  ) {
    this.clientForm = new FormGroup({
      id: new FormControl('', ),
      nome: new FormControl('', Validators.required),
      endereco: new FormControl('', Validators.required),
      ativo: new FormControl('', Validators.required),
      tipo: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([Validators.minLength(5), Validators.email])),
      senha: new FormControl('', Validators.required),
    });
  }

  get form() { return this.clientForm.controls; }

  ngOnInit() {
    this.getClients();

    this.interval = setInterval(() => {
      this.getClients();
    }, 5000);
  }

  getClients() {
    this.apiService.getClientes().subscribe(data => {
      this.listaDeClientes = data;
    }, error => {
      console.error(error);
    });
  }

  submit() {
    this.submited = true;

    if (this.clientForm.invalid) {
      return;
    } else {
      this.statusEnvio = true;
      this.cadastrarCliente();
    }
  }

  cadastrarCliente() {
    const body = this.clientForm.value;
    body.ativo = parseInt(body.ativo);

    console.log(body);

    if (localStorage.getItem('idCliente') === null) {
      this.apiService.cadastroCliente(body).subscribe(data => {
        console.log(data);
      }, error => {
        console.error(error);
      });
    } else {
      body.id = localStorage.getItem('idCliente');
      this.apiService.cadastroCliente(body).subscribe(data => {
        console.log(data);
      }, error => {
        console.error(error);
      });
    }

    alert('Salvo com sucesso!');

    if (!localStorage.getItem('idUsuario')) {
      this.router.navigate(['/login']);
    }
  }

  deleteClient(idCliente) {
    this.apiService.deleteCliente(idCliente).subscribe(data => {
      console.log(data);
    }, error => {
      console.error(error);
    });

  }

  editClient(idCliente) {
    this.apiService.getUmUsuario(idCliente).subscribe(data => {
      console.log(data);
      // tslint:disable: no-string-literal
      this.clientForm.controls['id'].setValue(data['id']);
      this.clientForm.controls['nome'].setValue(data['nome']);
      this.clientForm.controls['endereco'].setValue(data['endereco']);
      this.clientForm.controls['ativo'].setValue(String(data['ativo']));
      this.clientForm.controls['tipo'].setValue(data['tipo']);
      this.clientForm.controls['email'].setValue(data['email']);
      this.clientForm.controls['senha'].setValue(data['senha']);

      localStorage.setItem('idCliente', JSON.stringify(data['id']));
    }, error => {
      console.error(error);
    });
  }
}

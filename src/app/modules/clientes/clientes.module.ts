import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ListaClientesComponent } from './components/lista-clientes/lista-clientes.component';
import { LoginComponent } from './components/login/login.component';
import { CanActivateGuard } from './components/guards/guard.service';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [CanActivateGuard] },
  {
    path: 'clientes', component: LayoutComponent, canActivate: [CanActivateGuard], children: [
      { path: '', component: ListaClientesComponent },
    ]
  }
];

@NgModule({
  declarations: [ListaClientesComponent, LoginComponent],
  imports: [
    SweetAlert2Module.forRoot(),
    CommonModule,
    RouterModule.forChild(routes),
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [CanActivateGuard, LoginComponent]
})
export class ClientesModule { }

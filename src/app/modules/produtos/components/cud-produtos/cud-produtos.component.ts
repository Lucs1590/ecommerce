import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UtilService } from 'src/app/core/services/util.service';

@Component({
  selector: 'app-cud-produtos',
  templateUrl: './cud-produtos.component.html',
  styleUrls: ['./cud-produtos.component.css']
})
export class CudProdutosComponent implements OnInit {

  id: string;
  subscription: Subscription = new Subscription();
  produto = new Array();
  produtoForm: FormGroup;

  get form() { return this.produtoForm.controls; }

  imagem;

  file: File | null;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public apiService: ApiService,
    public formBuilder: FormBuilder,
    public utils: UtilService) {
    this.file = null;
  }

  ngOnInit() {

    this.produtoForm = this.formBuilder.group({
      id: [this.getId()],
      nome: ['', Validators.required],
      preco: ['', Validators.required],
      descricao: ['', Validators.required],
      tag: ['', Validators.required],
      file: ['']
    });

    this.id = this.getId();

    if (this.id) {
      this.getProduto(this.id);
    }
  }

  getId() {
    const id = this.route.snapshot.paramMap.get('id');
    return id;
  }

  getProduto(idProduto) {
    const getUmProduto = this.apiService.getUmProduto(idProduto).subscribe(data => {

      this.imagem = data['imagem'];

      // tslint:disable: no-string-literal
      this.produtoForm.controls['id'].setValue(data['id']);
      this.produtoForm.controls['nome'].setValue(data['nome']);
      this.produtoForm.controls['descricao'].setValue(data['descricao']);
      this.produtoForm.controls['preco'].setValue(data['preco']);
      this.produtoForm.controls['tag'].setValue(data['tag']);
      // this.produtoForm.controls['file'].setValue(data['imagem']);
    }, error => {
      console.error(error);
    });

    this.subscription.add(getUmProduto);
  }

  salvarProduto() {
    if (this.produtoForm.invalid) {
      console.log('Deu ruim no form');
      console.log(this.produtoForm);
      return;
    }

    const formData = new FormData();

    formData.append('id', this.getId());
    formData.append('nome', this.form.nome.value);
    formData.append('descricao', this.form.descricao.value);
    formData.append('preco', this.form.preco.value);
    formData.append('tag', this.form.tag.value);
    formData.append('file', this.file);

    this.apiService.updateAddProduto(formData).subscribe(data => {
      this.router.navigate(['/produtos']);
    });
  }

  onFileChange(event) {

    if (event.target.files && event.target.files[0]) {
      this.file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = e => this.imagem = reader.result;
      reader.readAsDataURL(this.file);
    }
  }
}

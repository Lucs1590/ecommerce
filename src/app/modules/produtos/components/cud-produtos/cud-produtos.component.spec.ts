import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CudProdutosComponent } from './cud-produtos.component';

describe('CudProdutosComponent', () => {
  let component: CudProdutosComponent;
  let fixture: ComponentFixture<CudProdutosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CudProdutosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CudProdutosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

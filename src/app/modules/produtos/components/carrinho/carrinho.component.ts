import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';
import { Subscription } from 'rxjs';
import { UtilService } from 'src/app/core/services/util.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-carrinho',
  templateUrl: './carrinho.component.html',
  styleUrls: ['./carrinho.component.css']
})
export class CarrinhoComponent implements OnInit {
  subscription: Subscription;
  produtos = new Array();
  listaDePedidos;
  quantidadeTotalItems;
  valorTotalAcumulado;
  interval;

  id_usuario;

  constructor(
    public apiService: ApiService,
    public utils: UtilService,
    private router: Router) {
      this.id_usuario = localStorage.getItem('idUsuario');
    }

  ngOnInit() {
    this.getPedidos();
  }

  getPedidos() {

    this.listaDePedidos = [];

    if (localStorage.getItem('carrinho')) {
      this.listaDePedidos = JSON.parse(localStorage.getItem('carrinho'));
      this.somarValorTotal(this.listaDePedidos);
      this.quantidadeTotalItems = this.listaDePedidos.length;
    }

    if (this.id_usuario) {
      this.apiService.getPedidos().subscribe(data => {

        if (!this.listaDePedidos) {
          this.listaDePedidos = [];
        }

        data.forEach(pedido => {
          this.listaDePedidos.push(pedido);
          this.somarValorTotal(this.listaDePedidos);

          this.quantidadeTotalItems = this.listaDePedidos.length;
        });
        console.log(this.listaDePedidos);

      }, error => {
        console.error(error);
      });
    }

  }

  somarValorTotal(listaDePedidos) {
    const preco = [];

    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < listaDePedidos.length; index++) {
      preco.push(listaDePedidos[index].preco * listaDePedidos[index].quantidade);
    }

    const reducer = (accumulator, currentValue) => accumulator + currentValue;

    this.valorTotalAcumulado = preco.reduce(reducer);
  }

  apagarPedido(i, idPedido) {

    this.listaDePedidos.splice(i, 1);

    if (this.id_usuario) {
      this.apiService.delPedido(idPedido).subscribe(data => {
        // this.getPedidos();
      }, error => {
        console.error(error);
      });
    } else {
      localStorage.setItem('carrinho', JSON.stringify(this.listaDePedidos));
      this.getPedidos();
    }
  }

  getTotal(event, pedido) {

    if (event) {
      pedido.quantidade = parseInt(event.srcElement.value);
    }

    let total = 0;

    if (this.listaDePedidos) {
      const precos = [];

      for (let index = 0; index < this.quantidadeTotalItems; index++) {

        const preco =   parseFloat(document.querySelectorAll('.precoPedido')[index].textContent.replace('$', '').trim());
        const quantidade = parseFloat(document.querySelectorAll('input')[index].value.replace('$', '').trim());

        // tslint:disable: radix
        precos.push(preco * quantidade);
      }

      const reducer = (accumulator, currentValue) => accumulator + currentValue;

      total = precos.reduce(reducer);
      return total;
    }
  }

  finalizarPedido() {
    if (this.id_usuario) {
      this.apiService.finalizarPedido(this.listaDePedidos).subscribe(data => {
        localStorage.removeItem('carrinho');
        setTimeout(() => {
          this.getPedidos();
        }, 500)
      }, error => {
        console.error(error);
      });
    } else {
      this.router.navigate(['/login']);
    }
  }
}

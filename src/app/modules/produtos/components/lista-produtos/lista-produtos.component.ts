import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/core/services/util.service';

@Component({
  selector: 'app-lista-produtos',
  templateUrl: './lista-produtos.component.html',
  styleUrls: ['./lista-produtos.component.css']
})
export class ListaProdutosComponent implements OnInit {
  subscription: Subscription;
  produtos;
  constructor(public apiService: ApiService, private router: Router, public utils: UtilService) { }

  ngOnInit() {
    this.utils.verificarLogado();
    this.getProdutos();
  }


  getProdutos() {
    this.apiService.getProduto().subscribe(data => {
      this.produtos = data;
    }, error => {
      console.error(error);
    });
  }

  apagarProduto(idProduto) {
    this.apiService.delProduto(idProduto).subscribe(data => {
      this.getProdutos();
    }, error => {
      console.error(error);
    });
  }

  adicionarCarrinho(produto) {
    if (localStorage.getItem('idUsuario')) {
      this.apiService.addPedido(produto.id, 1).subscribe(data => {
        console.log(data);
      }, error => {
        console.error(error);
      });
    } else {

      // PERSISTE CARRINHO EM LOCALSTORAGE
      // localStorage.removeItem('carrinho')
      let carrinho = [];
      if (localStorage.getItem('carrinho')) {
        carrinho = JSON.parse(localStorage.getItem('carrinho'));
      }
      produto.quantidade = 1;
      carrinho.push(produto);
      localStorage.setItem('carrinho', JSON.stringify(carrinho));

      this.router.navigate(['/produtos', 'carrinho']);

    }

  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaProdutosComponent } from './components/lista-produtos/lista-produtos.component';
import { CarrinhoComponent } from './components/carrinho/carrinho.component';
import { CudProdutosComponent } from './components/cud-produtos/cud-produtos.component';
import { CanActivateGuard } from '../clientes/components/guards/guard.service';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

const routes: Routes = [
  {
    path: 'produtos', component: LayoutComponent, canActivate: [CanActivateGuard], children: [
      { path: '', component: ListaProdutosComponent },
      { path: 'carrinho', component: CarrinhoComponent },
      { path: 'editar/:id', component: CudProdutosComponent },
      { path: 'adicionar', component: CudProdutosComponent }
    ]
  }
];

@NgModule({
  declarations: [ListaProdutosComponent, CarrinhoComponent, CudProdutosComponent],
  imports: [
    SweetAlert2Module.forRoot(),
    CommonModule,
    RouterModule.forChild(routes),
    RouterModule,
    ReactiveFormsModule
  ],
  providers: [CanActivateGuard]
})

export class ProdutosModule { }

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // private API_URL = 'http://10.64.207.14:3001';
  private API_URL = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  getHeader() {
    const usuario = JSON.parse(localStorage.getItem('currentUser'));
    return {
      headers: {
        Authorization: 'Bearer ' + (usuario ? usuario.token : '')
      }
    };
  }

  getProduto() {
    return this.http.get(`${this.API_URL}/produtos`, this.getHeader());
  }

  getPedidos() : any {
    let id_usuario = localStorage.getItem('idUsuario');
    if (!localStorage.getItem('idUsuario')) {
      id_usuario = 'provisorio';
    }
    return this.http.get(`${this.API_URL}/pedidos/usuario/${id_usuario}`, this.getHeader());
  }

  getUmProduto(id) {
    return this.http.get(`${this.API_URL}/produtos/${id}`, this.getHeader());
  }

  delProduto(id) {
    return this.http.delete(`${this.API_URL}/produtos/${id}`, this.getHeader());
  }

  addPedido(idProduto, quant) {

    return this.http.post(`${this.API_URL}/pedidos/`, {
      id_produto: idProduto,
      id_usuario: localStorage.getItem('idUsuario') ? localStorage.getItem('idUsuario') : 'provisorio',
      quantidade: quant
    }, this.getHeader());
  }

  delPedido(id) {
    return this.http.delete(`${this.API_URL}/pedidos/${id}`, this.getHeader());
  }

  login(body) {
    return this.http.post(`${this.API_URL}/usuarios/login`, body, this.getHeader());
  }

  cadastroCliente(body) {
    return this.http.post(`${this.API_URL}/usuarios`, body, this.getHeader());
  }

  updateAddProduto(body) {
    return this.http.post(`${this.API_URL}/produtos/`, body, this.getHeader());
  }

  insereUsuario(body) {
    return this.http.post(`${this.API_URL}/usuarios/`, body, this.getHeader());
  }

  getClientes() {
    return this.http.get(`${this.API_URL}/usuarios`, this.getHeader());
  }

  finalizarPedido(pedidos) {
    return this.http.post(`${this.API_URL}/pedidos/finalizar/usuario/${localStorage.getItem('idUsuario')}`, pedidos, this.getHeader());
  }

  deleteCliente(id) {
    return this.http.delete(`${this.API_URL}/usuarios/${id}`, this.getHeader());
  }

  getUmUsuario(id) {
    return this.http.get(`${this.API_URL}/usuarios/${id}`, this.getHeader());
  }
}

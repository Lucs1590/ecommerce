import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

declare var Chart: any;
@Injectable({
  providedIn: 'root'
})
export class UtilService {
  logadoAdmin = false;
  logadoComum = false;

  constructor() { }

  verificarLogado() {
    const admin = localStorage.getItem('permicao');
    const idUser = localStorage.getItem('idUsuario');
    if (idUser) {
      this.logadoComum = true;
      if (admin === 'admin') {
        this.logadoAdmin = true;
      }
    }
  }
}

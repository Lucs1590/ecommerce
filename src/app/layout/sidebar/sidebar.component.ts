import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UtilService } from 'src/app/core/services/util.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  clientId = null;

  constructor(public utils: UtilService) { }

  ngOnInit() {
    if (!this.subscription) {
      this.subscription = new Subscription();
    }
  }
  ngOnDestroy() { this.subscription.unsubscribe(); }
}

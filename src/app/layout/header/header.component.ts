import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService } from 'src/app/core/services/util.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {


  constructor(private router: Router, public utils: UtilService) { }
  nomeCliente: string;
  ngOnInit() {
    this.nomeCliente =  localStorage.getItem('nomeCliente');
  }

  ngOnDestroy() {
  }

  shutDown() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('selectedFile');
    localStorage.removeItem('idUsuario');
    localStorage.removeItem('permicao');
    localStorage.removeItem('nomeCliente');
    localStorage.clear();
    this.router.navigate(['/login']);
  }

}
